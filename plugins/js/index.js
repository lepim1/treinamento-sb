// Trim para manter compatibilidade com IE8
if(typeof String.prototype.trim !== 'function') {
	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g, ''); 
	}
}
function saveVitrineState(url) {
	history.pushState({vitrine:$('#vitrine')[0].outerHTML}, "", url);					
}

$( document ).ready(function() {
	saveVitrineState(document.url);

	// Pesquisa por produtos do topo da pagina
	$("#search-top-go").click(function(event ) {
		event.preventDefault();
		var form = $("#search-website-1");
		submitFormSearchProduct(form);
	});

	// Pesquisa por produtos do fim da pagina
	$("#search-bottom-go").click(function(event ) {
		event.preventDefault();
		var form = $("#search-website-2");
		submitFormSearchProduct(form);
	});

	// Submit formulario com a tecla enter
	$('.formCancelSubmit').on('submit', function(e) {
		submitFormSearchProduct($(this));
		return false;
	});

	// Evento que atualiza a vitrine e a paginacao atual quando o usuario clica na paginacao
	$( "body" ).delegate(".page-item", "click", function(e) {
		e.preventDefault();
		var addressValueAjax = $(this).attr("href-ajax");
		var addressValue = $(this).attr("href");
		$.get(addressValueAjax).done(function(result) {
			replaceVitrineWith(result)
			saveVitrineState(addressValue);	
		}).fail(function(){
			alert("Erro");
		});
		return false;
	});

	// Envia formulario para o servidor realizar a busca por produtos
	function submitFormSearchProduct(form) {
		var val = $(form).find('input[name="query-product"]').val();
		var urlAjax = $(form).attr("action-ajax");
		var url = $(form).attr("action");
		if (val.trim()!="") {			
			var data = $(form).serialize();			
			$.ajax({
				type: $(form).attr("method"),
				url: urlAjax,
				data: data,
			}).done(function(result) {				
				$('input[name="query-product"]').val("");
				replaceVitrineWith(result)
				$('html, body').animate({ scrollTop: $('#vitrine').offset().top/2 }, 'slow');
				saveVitrineState(url);
			}).fail(function(){
				alert("Erro");
			});
		}
	}

	function replaceVitrineWith(content) {
		$("#vitrine").replaceWith(content);
	}

	window.onpopstate = function(event) {
		replaceVitrineWith(event.state.vitrine);
	}

});