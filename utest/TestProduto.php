<?php
require_once('../model/produto.php');
class TestProduto extends PHPUnit_Framework_TestCase
{
    private $p;

    public function setUp() {
        $this->p = new Produto();
        $this->p->readFromJSON(); 
    }
    
     /**
     * @dataProvider searchProductProvider
     */
    public function testSearchProduct($query, $expected) {        

        $results = $this->p->searchProduct($query, 1);
        $this->assertEquals($expected, $results["resultsFound"]);                
    }

    public function searchProductProvider() {
         return array(
         array("Mortal", 6),
         array("mortal", 6),
         array("mortal10", 0),
         array("", 12),
         array("test", 0),
         array("teste", 0),
         array("relógio", 1));
    }

}