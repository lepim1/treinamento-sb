<div class="row" id="vitrine">

<?php
	if ($results['resultsFound']==0) {
?>
			<div class="col-xs-12 col-sm-6 col-md-3 product-column">
				<span class="price"> 0 results found </span>
			</div>
<?php 	
	} else {
		foreach ($results['resultsPaginado'] as $key => $value) { 
?>
			<div class="col-xs-12 col-sm-6 col-md-3 product-column">
		        <a href="<?php echo $value['Foto']; ?>"><img src="<?php echo $value['Foto']; ?>" alt="Secured" class="img-produto" ></a>
		        <h5 class="product-name"><?php echo $value['Nome']; ?></h5>
		        <span>Price: <span class="price"> $<?php echo number_format($value['PrecoPor'], 2); ?></span></span>
		  	</div><!--/.col-xs-6.col-lg-3-->
<?php   
		} 
	} 
?>
			<div class="col-xs-12 col-sm-12 col-md-12">		        		        
		        <?php include_once('paginacao-view.php'); ?>
		  	</div>
        
</div><!--/row-->