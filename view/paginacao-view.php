<nav class="text-center" style="border-bottom: 1px solid #CFD0CA;" id="paginacao">
	<ul class="pagination pagination-sm">

<?php
	$nPaginas = ceil($results['resultsFound']/Produto::PRODUTO_POR_PAGINA_SEARCH);
	for ($i=1; $i<= $nPaginas; $i++) { 
?>
		<?php
			if ($i == $paginacao) {
		?>
				<li class="active">
		<?php
			} else {				
		?>
				<li>
	<?php
			} 
	?> 
					<a class="no-border page-item" href="<?php echo 'index.php?query-product='.$query.'&p='.$i; ?>" href-ajax="<?php echo '../controller/search-product-controller.php?query-product='.$query.'&p='.$i; ?>"><?php echo $i; ?> <span class="sr-only">(current)</span></a>
		    	</li>
<?php 
	} 
?>

	</ul>
</nav>