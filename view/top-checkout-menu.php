<!-- [INICIO] MENU DE BUSCA E CHECKOUT -->
<div class="container red-background">
  <div class="row row-custom">

    <div class="col-sm-1 col-md-4 col-lg-4">          
      <div class="row">
        <div class="col-sm-2 col-md-2">          
          <img src="<?php echo $image_path.'sacola2.png'; ?>" alt="Sacola" width="60" height="70">            
        </div>
        <div class="col-sm-8 col-md-8 col-lg-6 text-center" style="margin-top:10px;">
          <p class="cor-font-logo" style="font-size:16px;"><span id="items-in-your-bag">0</span> items in your bag</p>
          <button type="submit" class="btn btn-checkout">Check Out</button>
        </div>
        <div class="col-sm-1 col-md-1 col-lg-4">            
        </div>
      </div>
    </div>        

    <div class="col-sm-1 col-md-2 col-lg-4 text-center cor-font-logo dimensao-font-logo" >
      ECOMMERCE WEBSITE          
    </div>
    <div class="col-sm-1 col-md-3 col-lg-4 pull-right">
      <form id="search-website-1" method="GET" action-ajax="../controller/search-product-controller.php" action="/view/index.php" class="formCancelSubmit">
        <div class="input-group input-group-right-aligned" style="top:30px;">
          <input type="text" class="form-control" placeholder="Search website" name="query-product">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button" name="go" id="search-top-go">GO</button>
          </span>
        </div>
      </form>
    </div>
  </div><!-- /div .row -->
</div><!-- /container .red-background -->
<!-- [FIM] MENU DE BUSCA E CHECKOUT -->