<footer>
  <div class="container background-black">       
    <div class="row row-custom">
      <div class="col-md-4">
        <img src="<?php echo $image_path. 'secured.png'; ?>" alt="Secured" style="width:60px;height:55px;border:0;">
        <span class="footer-text-logo">Shop online with us safely and securely</span>
      </div>
      <div class="col-md-4 text-center">
        <img src="<?php echo $image_path. 'shipping-box.png'; ?>" alt="Shipment" class="img-48x48">
        <span class="footer-text-logo">We ship your orders anywhere</span>                  
      </div>
      <div class="col-md-4">
        <form id="search-website-2" method="GET" action-ajax="../controller/search-product-controller.php" action="action-ajax" class="formCancelSubmit">
          <div class="input-group input-group-right-aligned" style="top:10px;">
            <input type="text" class="form-control" placeholder="Search website" name="query-product">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button" name="go" id="search-bottom-go">GO</button>
            </span>
          </div>          
        </form>
      </div>
    </div>
  </div>

  <div class="container bg-footer">
    <div class="row">
      <div class="col-md-2">
        <ul>
          <li><a href="">Company</a></li>
          <li><a href="">Home</a></li>
          <li><a href="#">About us</a></li>
          <li><a href="#">Blog</a></li>
          <li><a href="#">Latest News</a></li>
          <li><a href="#">Login</a></li>
          <li><a href="#">Join us</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <ul>
          <li><a href="">Lorem ipslum dolor sit</a></li>
          <li><a href="">Amet consectetur</a></li>
          <li><a href="#">Adisciplit elit</a></li>
          <li><a href="#">Cras suscipit lacus</a></li>
          <li><a href="#">Latest News</a></li>
          <li><a href="#">Login</a></li>
          <li><a href="#">Join us</a></li>
        </ul>
      </div>
      <div class="col-md-2">
        <ul>
          <li><a href="">Categories</a></li>
          <li><a href="">Home</a></li>
          <li><a href="#">About us</a></li>
          <li><a href="#">Blog</a></li>
          <li><a href="#">Latest News</a></li>
          <li><a href="#">Login</a></li>
          <li><a href="#">Join us</a></li>
        </ul>
      </div>
      <div class="col-md-2">
        <ul>
          <li><a href="">Company</a></li>
          <li><a href="">Home</a></li>
          <li><a href="#">About us</a></li>
          <li><a href="#">Blog</a></li>
          <li><a href="#">Latest News</a></li>
          <li><a href="#">Login</a></li>
          <li><a href="#">Join us</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <ul>
          <li><a href="">Company</a></li>
          <li><a href="">Phone: 1.234.567.8901</a></li>
          <li><a href="#">Toll-Free: 1.234.567.8901</a></li>
          <li><a href="#">Faz: 1.234.567.8901</a></li>
          <li><a href="#">Email: <span>Send us an email</span></a></li>
          <li>&nbsp;</li>
          <li><a href="#" class="font-bold">MON - SAT 9am to 7:30pm</a></li>
          <li><a href="#" class="font-bold">Sundays, holidays closed</a></li>
        </ul>
      </div>
    </div>
  </div>
</footer>