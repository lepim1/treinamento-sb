<!-- [INICIO] CARROSSEL --> 
<h4 class="featured-product-title">Featured Products</h4>
<div class="row border-featured-product">
  <button class="bt-carrossel bt-next">
    <div class="arrow-right"></div>
    <div class="arrow-diagonal-left"></div>
  </button >

  <button class="bt-carrossel bt-prev">
    <div class="arrow-left"></div>
    <div class="arrow-diagonal-right"></div>
  </button >     
  <div class="slider responsive slider-style">
  <?php
    foreach ($product->getArrProduto() as $key => $value) {
      if ($value["Disponivel"] > 0) {
  ?>
        <div><a href=""><img src="<?php echo $value['Foto']; ?>" alt="" class="margin-auto img-produto"><span class="featured-product-description"><?php echo $value['Nome']; ?></span></a></div>          
          
  <?php 
      }
    } 
  ?>
  </div>
</div><!-- [FIM] CARROSSEL --> 