<?php
include_once('../config.php');
$image_path = Config::IMAGE_PATH;
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>E-commerce</title>

  <!-- Bootstrap -->
  <link href="../plugins/bootstrap-3.3.5/css/bootstrap.min.css" rel="stylesheet">

  <link href="../plugins/bootstrap-3.3.5/css/bootstrap-custom.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<link rel="stylesheet" type="text/css" href="../plugins/slick-1.5.7/slick/slick.css"/>
</head>
<body>
  <?php
  include_once('top-contact-menu.php');
  include_once('top-checkout-menu.php');
  include_once('top-product-menu.php');
  include_once('sale-banner.php');
  ?>

  <!-- [INICIO] MIOLO DA PÁGINA (LISTAGEM DE MENUS LATERAIS E DOS PRODUTOS) -->    
  <header class="container">
    <div class="row row-offcanvas row-offcanvas-right">

      <!-- [INICIO] MENU LATERAL -->    
      <div class="col-xs-6 col-sm-2 sidebar-offcanvas" id="sidebar">

        <!-- [INICIO] PRIMEIRO MENU LATERAL (BROWSE CATEGORIES) --> 
        <div class="list-group">
          <span class="list-group-item list-group-item-custom list-group-item-header">Browse Categories</span>
          <a href="#" class="list-group-item list-group-item-custom list-group-item-top">Lorem ipsum dolor sit</a>
          <a href="#" class="list-group-item list-group-item-custom">Pellentesque commodo</a>
          <a href="#" class="list-group-item list-group-item-custom">Etiam quis elit risus</a>
          <a href="#" class="list-group-item list-group-item-custom">Morbi gravida cursus</a>
          <a href="#" class="list-group-item list-group-item-custom">Quisque tincidunt, est</a>
          <a href="#" class="list-group-item list-group-item-custom">Aliquam cursus nibh</a>
          <a href="#" class="list-group-item list-group-item-custom">Donec metus libero</a>
          <a href="#" class="list-group-item list-group-item-custom">In luctus convallis</a>
          <a href="#" class="list-group-item list-group-item-custom list-group-item-bottom">Aenean eu lorem </a>
        </div><!-- /.list-group -->
        <!-- [FIM] PRIMEIRO MENU LATERAL --> 

        <!-- [INICIO] SEGUNDO MENU LATERAL (REDES SOCIAIS) --> 
        <div id="social-container" class="list-group">            
          <span class="list-group-item list-group-item-custom-social">Join our newsletter list to get the latest updates</span>

          <form method="post" action="#" class="list-group-item list-group-item-custom-social text-center formCancelSubmit" name="form-join-us">              
            <input type="text" class="form-control" placeholder="">                            
            <button type="submit" class="btn btn-default" style="margin-top:5px;">Join Now</button>
          </form>

          <a href="#" class="list-group-item list-group-item-custom-social list-group-item-top">
            <div class="row">
              <div class="col-md-2" >
                <img src="<?php echo $image_path. 'twitter-logo.png'; ?>" alt="Follow us on Twitter" class="img-32x32">
              </div>
              <div class="col-md-10 padding-left-25px">
                <span>Follow us on Twitter</span>
              </div>
            </div>
          </a>
          <a href="#" class="list-group-item list-group-item-custom-social">
            <div class="row">
              <div class="col-md-2">
                <img src="<?php echo $image_path. 'fb-logo.png'; ?>" alt="Become our fan on Facebook" class="img-32x32">
              </div>
              <div class="col-md-10 padding-left-25px">
                <span>Become our fan on Facebook</span>
              </div>
            </div>
          </a>
          <a href="#" class="list-group-item list-group-item-custom-social">
            <div class="row">
              <div class="col-md-2">
                <img src="<?php echo $image_path. 'linkedin-logo.png'; ?>" alt="Connect with us on Linkedin" class="img-32x32">
              </div>
              <div class="col-md-10 padding-left-25px">
                <span>Connect with us on Linkedin</span>
              </div>
            </div>
          </a>
          <a href="#" class="list-group-item list-group-item-custom-social">
            <div class="row">
              <div class="col-md-2">
                <img src="<?php echo $image_path. 'email.png'; ?>" alt="Send us your email enquiries" class="img-32x32">
              </div>
              <div class="col-md-10 padding-left-25px">
                <span>Send us your email enquiries</span>
              </div>
            </div>
          </a>            
        </div>
        <!-- [FIM] SEGUNDO MENU LATERAL (REDES SOCIAIS) --> 

        <!-- [INICIO] TERCEIRO MENU LATERAL (BANDEIRAS) --> 
        <div id="social-container2" class="list-group">                        
          <a href="#" class="list-group-item list-group-item-custom-social list-group-item-top">
            <img src="<?php echo $image_path. 'paypal.png'; ?>" alt="PayPal" class="img-100pc">
          </a>
          <a href="#" class="list-group-item list-group-item-custom-social list-group-item-top">
            <img src="<?php echo $image_path. 'google-checkout.gif'; ?>" alt="Google Checkout" class="img-100pc">
          </a>
          <a href="#" class="list-group-item list-group-item-custom-social list-group-item-top text-center">
            <img src="<?php echo $image_path. 'mcafee.png'; ?>" alt="McAfee" style="width:91px;height:55px;border:0;">
          </a>
        </div>
        <!-- [FIM] TERCEIRO MENU LATERAL (BANDEIRAS) --> 

      </div><!--/.sidebar-offcanvas--> 
      <!-- [FIM] MENU LATERAL -->    




      <!-- [INICIO] SORT BY MENU E LISTAGEM DE PRODUTOS --> 
      <div class="col-xs-6 col-sm-9">          

        <!-- [INICIO] SORT BY MENU --> 
        <div class="row">
          <div class="col-md-1" style="padding-right:0px;">
            <span> Sort by:</span>
          </div>                          
          <div class="col-md-2">
            <select class="form-control">
              <option>Ascending</option>                                           
            </select> 
          </div>
          <div class="col-md-3">
            <select class="form-control">
              <option>Product Name</option>                
            </select> 
          </div>
          <div class="col-md-2">
            <select class="form-control">
              <option>Brand Name</option>
            </select> 
          </div>
          <div class="col-md-4 text-right">
            <span>Items per page: 12 / 20 / 30 / 50</span>
          </div>          
        </div>
        <!-- [FIM] SORT BY MENU --> 


        <!-- [INICIO] LISTAGEM DE PRODUTOS --> 
        <div class="row" style="margin-top:10px;">        
          <div class="col-md-3">
            <h4 class="our-products">OUR PRODUCTS</h4>
          </div>
        </div>

        <?php 
          include_once('../controller/search-product-controller.php');
          
          include_once("carrossel.php");
        ?>


      </div><!--/.col-xs-12.col-sm-9-->
      <!-- [FIM] SORT BY MENU E LISTAGEM DE PRODUTOS --> 

    </div><!--/row-offcanvas row-offcanvas-right-->
  </header><!--/container-->
  <!-- [FIM] MIOLO DA PAGINA (LISTAGEM DE MENUS LATERAIS DOS PRODUTOS) -->    


  <?php  
  include_once("footer.php");
  ?>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="../plugins/jquery-1.11.3.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="../plugins/bootstrap-3.3.5/js/bootstrap.min.js"></script>

  <!-- Slick (Carrosel)-->
  <script type="text/javascript" src="../plugins/slick-1.5.7/slick/slick.min.js"></script>

  <script>
    $('.responsive').slick({
      arrows: false,
      dots: false,
      infinite: true,
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 4,
      responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
        ]
      });

    $(".bt-prev").on("click", function(event) {  
      event.preventDefault();
      $(".responsive").slick('slickPrev');
    });

    $(".bt-next").on("click", function(event) {  
      event.preventDefault();
      $(".responsive").slick('slickNext');
    });
  </script>

  <script src="../plugins/js/index.js"></script>

</body>
</html>