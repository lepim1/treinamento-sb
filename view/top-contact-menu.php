<!-- [INICIO] MENU MAIS AO TOPO -->
 <nav class="container navbar navbar-default navbar-inverse navbar-top-custom">  
  <div id="navbar" class="collapse navbar-collapse">
    <ul class="nav navbar-nav">
      <li><a href="#" class="skiny-link">Account Sign In</a></li>
      <li><a href="#" class="skiny-link">Register</a></li>
      <li><a href="#" class="skiny-link">Buyers Guide</a></li>
      <li><a href="#" class="skiny-link">About</a></li>
      <li><a href="#" class="skiny-link">Blog</a></li>
      <li><a href="#" class="skiny-link">Contact</a></li>                        
    </ul>

    <ul class="nav navbar-nav pull-right" >
      <li><a href="#" class="skiny-link"><img src="<?php echo $image_path. 'orange-phone-3.png'; ?>" class="img-16x16" alt="Call">123.456.7890</a></li>
      <li><a href="#" class="skiny-link"><img src="<?php echo $image_path. 'chat-4.png'; ?>" alt="Phone receiver" class="img-16x16"> Live Help</a></li>            
      <li><a href="#" class="skiny-link flags"><img src="<?php echo $image_path. 'canada.png'; ?>" alt="Canada" class="img-16x16" ></a></li>
      <li><a href="#" class="skiny-link flags"><img src="<?php echo $image_path. 'usa.png'; ?>" alt="USA" class="img-16x16" ></a></li>
    </ul>
  </div><!-- /.nav-collapse -->              
</nav><!-- /.navbar -->
<!-- [FIM] MENU MAIS AO TOPO -->