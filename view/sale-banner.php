<!-- [INICIO] BANNER DA PÁGINA -->    
<div class="container">
  <div class="row row-sale">
    <div class=" col-xs-12 col-sale">
      <img src="<?php echo $image_path. 'banner-sale2.png'; ?>" alt="Banner Sale" style="width:100%;height:252px;border:0;">        
    </div>    
  </div>

  <div class="row product-title-row">
    <div class="col-xs-3 text-center product-title-column">
      <span class="product-title-text">PRODUCT TITLE</span>
    </div>

    <div class="col-xs-9 product-title-description-column">
      <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum dapibus purus, nec vestibulum ipsum vulputate id. Integer sit amet eleifend ante.</span>
    </div>
  </div>
</div>
<!-- [FIM] BANNER DA PÁGINA -->   