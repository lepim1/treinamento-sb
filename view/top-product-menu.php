<!-- [INICIO] MENU DE SESSÕES DE PRODUTOS -->
<nav class="container navbar navbar-inverse navbar-inverse-custom">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div id="navbar2" class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#" class="medium-link">HOME</a></li>
        <li><a href="#" class="medium-link">latest arrivals</a></li>
        <li><a href="#" class="medium-link">MEN'S</a></li>
        <li><a href="#" class="medium-link">KIDS</a></li>
        <li><a href="#" class="medium-link">BRANDS</a></li>
        <li><a href="#" class="medium-link">MEN'S</a></li>
        <li><a href="#" class="medium-link">SALE</a></li>
        <li><a href="#" class="medium-link">GIFT CARDS</a></li>
        <li><a href="#" class="medium-link">FREEBIES</a></li>
      </ul>
    </div><!-- /.nav-collapse -->
  </div><!-- /.container -->
</nav><!-- /.navbar -->
<!-- [FIM] MENU DE SESSÕES DE PRODUTOS -->