<?php
	require_once('../model/produto.php');

	//Pegar a paginacao atual. Se a paginacao nao for definida ou <= 0, use a primeira pagina como default
	$paginacao = 1;	
	if (isset($_GET['p'])) {
		$paginacao = $_GET['p'];
		if($paginacao <= 0) {
			$paginacao = 1;
		}
	}

	//Pegar a consulta do usuario. Se nao existir assuma "" como default
	$query = "";	
	if (isset($_GET['query-product'])) {
		$query = $_GET['query-product'];
	}
	
	$product = new Produto();
	$product->readFromJSON();
	
	//Busca produto na variavel que contem todos os produtos
	$results = $product->searchProduct($query, $paginacao);

	//exibe produtos encontrados
	include_once('../view/vitrine-view.php');		

	// //exibe paginacao
	// include_once('../view/paginacao-view.php');	

?>