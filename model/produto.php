<?php

/**
* Classe de modelo do produto
*/
class Produto {
	private $ArrProduto;					//produtos lidos da base de dados
	const PRODUTO_POR_PAGINA_SEARCH = 5;	//paginar de 5 em 5

	//Leitura base de dados dos produtos em formato JSON
	public function readFromJSON(){
		$str = file_get_contents('../Produtos.json');
		$json_products = json_decode($str);
		foreach ($json_products->Produto as $key => $value) {
			$this->setParamJson($value);
		}
	}

	/**
	* 	Recebe um JSON e insere-o em um array
	*/
	public function setParamJson($value) {
		$this->ArrProduto[] = array(
			"ProdutoId" => $value->ProdutoId, 
			"Nome" => $value->Nome,
			"PrecoPor" => $value->PrecoPor,
			"Status" => $value->Status,
			"Foto"=>$value->Foto,
			"Disponivel"=> $value->Estoque->Disponivel);
	}

	public function getArrProduto() {
		return $this->ArrProduto;
	}

	/**
	*	Recebe uma string de consulta e o numero da paginacao e retorna um vetor contendo os resulktados da busca. Ex.: array(resultsFound=>12, resultsPaginado=>[Produtos])
	*/
	public function searchProduct($query, $paginacao) {
		$query = $this->prepareStringToSearch($query);
		$result = array("resultsFound" => 0, "resultsPaginado" => array());
		if(empty($query)){	// Busca nao definida pelo usuario. Retorna os N produtos da primeira paginacao 
			foreach ($this->ArrProduto as $key => $value) {
				$this->verificaStatusAddPaginado($value, $result, $paginacao);
			}
		}else{	// Busca definida pelo usuario. Retorna os produtos encontrados na paginacao correspondente 
			foreach ($this->ArrProduto as $key => $value) {
				$produtoId = $this->prepareStringToSearch($value["ProdutoId"]);
				$produtoNome = $this->prepareStringToSearch($value["Nome"]);
				if ((strpos($produtoId, $query) !== false || strpos($produtoNome, $query) !== false) ) {
					$this->verificaStatusAddPaginado($value, $result, $paginacao);
				}
			}
		}		
		return $result;
	}

	/**
	*	Verifica status do produto e adiciona somente os produtos da paginacao correspondente
	*/
	function verificaStatusAddPaginado($value, &$result, $paginacao) {
		if($value["Disponivel"] > 0){
			$result["resultsFound"] += 1;				
			if(count($result["resultsFound"]) < self::PRODUTO_POR_PAGINA_SEARCH){
				$paginacaoAux = $result["resultsFound"]-1;
				if ($paginacaoAux >= ($paginacao*self::PRODUTO_POR_PAGINA_SEARCH-self::PRODUTO_POR_PAGINA_SEARCH) && $paginacaoAux < ($paginacao*self::PRODUTO_POR_PAGINA_SEARCH)){
					$result["resultsPaginado"][] = $value;
				}
			}
		}
		return $result;
	}

	/**
	* Prepara uma string para busca. Ex.: Relógio torna relogio (sem acento, lower case e trimmed)
	*/
	function prepareStringToSearch($string) {
		return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '', preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8'))), ' '));
	}

}
